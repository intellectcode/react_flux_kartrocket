var $ = require("jquery");
var initDashboard = function(callback) {
    var categories = {
        "Men": {
            "t_shirt": "Tshirt",
            "jeans": "Jeans"
        },
        "Women": {
            "legging": "Legging",
            "kurti": "Kurti"
        }
    }
    var staticContent = {
        "home_banner": "https://www.w3schools.com/css/img_fjords.jpg"
    }
    var products = {
        Men: {
            t_shirt: [{
                id: "1",
                name: "Green Polo t shirt",
                price: 2499,
                image: "http://static4.jassets.com/p/Nautica-Green-Polo-T-Shirt-3105-445817-1-catalog_s.jpg"
            }, {
                id: "11",
                name: "V Neck T Shirt",
                price: 2499,
                image: "http://static1.jassets.com/p/Gritstones-Maroon-Solid-Henley-T-Shirt-8233-1616182-1-catalog_s.jpg"
            }],
            jeans: [{
                id: "2",
                name: "Lee jeans",
                price: 1499,
                image: "http://static3.jassets.com/p/Jack---Jones-Blue-Mid-Rise-Regular-Fit-Jeans-7130-4007072-1-catalog_xs.jpg"
            }, {
                id: "22",
                name: "Jack & Jones",
                price: 1499,
                image: "http://static3.jassets.com/p/Wrangler-Blue-Low-Rise-Slim-Fit-Jeans-1162-6580672-1-catalog_xs.jpg"
            }]
        },
        Women: {
            legging: [{
                id: "33",
                name: "Black leggings",
                price: 1499,
                image: "http://static1.jassets.com/p/De-Moza-White-Solid-Leggings-9455-4043402-1-catalog_s.jpg"
            }, {
                id: "44",
                name: "Black leggings",
                price: 1499,
                image: "http://static1.jassets.com/p/Swaron-Blue-Solid-Leggings-2827-6128982-1-catalog_s.jpg"
            }, {
                id: "3",
                name: "Black leggings",
                price: 1499,
                image: ""
            }, {
                id: "3",
                name: "Black leggings",
                price: 1499,
                image: "http://static1.jassets.com/p/Swaron-Blue-Solid-Leggings-2827-6128982-1-catalog_s.jpg"
            }],
            kurti: [{
                id: "55",
                name: "Blue Kurti",
                price: 1499,
                image: "http://static2.jassets.com/p/Melange-by-Lifestyle-Fuchsia-Solid-Kurti-8934-446100003-1-catalog_s.jpg"
            }, {
                id: "55",
                name: "Blue Kurti",
                price: 1499,
                image: "http://static2.jassets.com/p/Vedic-Aqua-Blue-Printed-Cotton-Kurta-6836-893400003-1-catalog_s.jpg"
            }]
        }
    }
    callback({
        staticContent: staticContent,
        categories: categories,
        products: products
    });
}
module.exports = {
        initDashboard: initDashboard
    }
    //var $ = require("jquery");
    //
    //var initDashboard = function(callback) {
    //    var categories = {
    //        "Men": {
    //            "t_shirt": "Tshirt",
    //            "jeans": "Jeans"
    //        },
    //        "Women": {
    //            "legging": "Legging",
    //            "kurti": "Kurti"
    //        }
    //    }
    //    
    //    var staticContent = {
    //        "home_banner": "https://www.w3schools.com/css/img_fjords.jpg"
    //    }
    //
    //    var products = {
    //        Men: {
    //            t_shirt: [{id:"1", name: "Men Polo t shirt", price:2499}],
    //            jeans: [{id:"2", name: "Lee jeans", price:1499}],
    //        },
    //        Women: {
    //            legging: [{id:"3", name: "Black leggings", price:1499}],
    //            kurti: [{id:"4", name: "Blue Kurti", price:1499}]
    //        }
    //    }
    //    
    //    callback({
    //        staticContent: staticContent,
    //        categories: categories,
    //        products: products
    //    });
    //}
    //
    //module.exports = {
    //    initDashboard: initDashboard
    //}
