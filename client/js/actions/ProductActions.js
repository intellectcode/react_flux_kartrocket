var ProductApi = require("../utils/ProductApi.js");
var AppDispatcher = require("../dispatcher/AppDispatcher");
var ProductConstants = require("../constants/ProductConstant");

var initDashboard = function(){
    ProductApi.initDashboard(function(result){
        initializeDashboard(result);
    });
}

var initializeDashboard = function(data) {
    AppDispatcher.handleAction({
        actionType: ProductConstants.INIT_DASHBOARD,
        data: data
    })
}
var loadProducts = function(data) {
    AppDispatcher.handleAction({
        actionType: ProductConstants.LOAD_PRODUCTS,
        data: data
    })
}

module.exports = {
    initDashboard: initDashboard,
    loadProducts: loadProducts,
};