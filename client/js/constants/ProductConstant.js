var keyMirror = require('keymirror');

module.exports = keyMirror({
    INIT_DASHBOARD: null,
    LOAD_PRODUCTS: null
});