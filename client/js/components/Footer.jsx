var React = require("react");

var Footer = React.createClass({
    getInitialState: function() {
        return  {
        }
    },
    render: function() {
        return(
        <footer className="footer">
            <div className="container footer-links">
                <div className="center footer-brand">
                    <img src="../images/logo.png"/>
                </div>
                <div className="footer-content">
                    <div className="col-md-4 col-wraper">
                        <ul className="footer-list">
                            <li>
                                <span className="footer-left-menu col-text"><a href="#">ABOUT US</a></span>
                                <span className="footer-left-menu col-text"><a href="#">CONTACT US</a></span>
                                <span className="footer-left-menu col-text"><a href="#">HELP</a></span>
                            </li>
                            <li>
                                <span className="footer-left-menu col-text">Payment Options:</span>
                                <span className="footer-left-menu"><img className="payment-badge-icon" src="../images/cod.png" /></span>
                                <span className="footer-left-menu"><img className="payment-badge-icon" src="../images/visa.png" /></span>
                                <span className="footer-left-menu"><img className="payment-badge-icon" src="../images/master-card.png" /></span>
                            </li>
                        </ul>
                    </div>
                    <div className="center col-md-4 col-wraper">
                        
                    </div>
                    <div className="col-md-4 col-wraper">
                        <ul className="footer-list contact-info">
                            <li><img className="footer-right-icons" src="../images/call.png"/>Call Us at 9582051356</li>
                            <li><img className="footer-right-icons" src="../images/whatsapp.png"/>WhatsApp Us at 9582051356</li>
                            <li><img className="footer-right-icons" src="../images/email.png"/>Email Us at niravkapoor27@gmail.com</li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        )
    }
});

module.exports = Footer;