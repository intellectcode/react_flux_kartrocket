var React = require("react");
var ProductActions = require("../actions/ProductActions");

var Header = React.createClass({
    getInitialState: function() {
        return  {
            categories: {}
        }
    },
    componentDidMount: function() {
        this.setState({
            initialStateData: this.props.initialStateData,
            categories: this.props.initialStateData.categories
        });
    },
    componentWillReceiveProps: function(nextProps) {
        this.setState({
            initialStateData: nextProps.initialStateData,
            categories: nextProps.initialStateData.categories
        });
    },
    showProducts: function(category, subCategory){
        var initialStateData = this.state.initialStateData;
        var prod = initialStateData.products[category][subCategory];
        ProductActions.loadProducts(prod);
    },
    createCategoriesHtml: function() {
        var that = this;
        var categories = [];
        var categoriesObj = this.state.categories;
        if(!categoriesObj)
            return categories;
        
        var getSubCategoryEle = function(obj, id){
            var ele = [], subEle = [];
            for(var key in obj){
                subEle.push(<li key={key}><span onClick={that.showProducts.bind(null,  id, key)}>{obj[key]}</span></li>);
            }
            ele.push(<div key={key} id={id} className="collapse">{subEle}</div>);
            return ele;
        }
        
        for(var key in categoriesObj){
            categories.push(
                <li key={key}>
                    <a><div data-toggle="collapse" data-target={'#'+key}><strong>{key}</strong></div>
                        <ul className="nav navbar-nav">
                            {getSubCategoryEle(categoriesObj[key], key)}
                        </ul>
                    </a>
                </li>
            );
        }
        
        for(var key in categoriesObj){
            categories.push();
        }
        
        return categories;
    },
    render: function() {
        return(
            <div>
            <nav className="navbar navbar-default navbar-fixed-top navbar-shadow" >
                <div className="container">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#/"><img src="./images/logo.png"/></a>                       
                    </div>
                    <div id="navbar" className="collapse navbar-collapse navbar-right navbar-main-collapse">
                        <div className="collapse navbar-collapse navbar-right navbar-main-collapse">
                            <ul className="nav navbar-nav">
                                {this.createCategoriesHtml()}
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <main>
                <div className="container">                    
                </div>
                <div className="container">
                    <div className="row tags">                                                                  
                    </div>
                </div>
                <div className="price-rise-fall">
                </div>
            </main>
            <div className="overlay">
                <div className="profile-overlay"></div>
            </div>
        </div>
        )
    }
});

module.exports = Header;