var React = require("react");
var ProductStore = require("../stores/ProductStore");
var ProductActions = require("../actions/ProductActions");


var Products = React.createClass({
    getInitialState: function() {
        return  {
            initialStateData: {},
            products: []
        }
    },
    componentDidMount: function() {
        this.setState({
            initialStateData: this.props.initialStateData,
            products: this.props.products,
            staticContent: this.props.staticContent
        });
        this.bindEvents();
    },
    componentWillReceiveProps: function(nextProps) {
        this.setState({
            initialStateData: nextProps.initialStateData,
            products: nextProps.products,
            staticContent: nextProps.staticContent
        });
    },
    componentWillUnmount: function() {
       window.removeEventListener('scroll');
    },
    bindEvents: function(){
        window.addEventListener('scroll', this.onScroll);
    },
    onScroll: function() {
        if ((window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 100) && this.state.products.length < 20) {
            // make api hit to get products.
            var prod = this.state.products;
            prod.push(prod[0]);
            ProductActions.loadProducts(prod);
        }
    },
    showProducts: function(category, subCategory){
        var initialStateData = this.state.initialStateData;
        var prod = initialStateData.products[category][subCategory];
        ProductActions.loadProducts(prod);
    },
    createProductHtml: function () {
        var products = this.state.products, productsElements = [];

        products.map(function(prod, index){
            productsElements.push(
                <div className="col-md-3 card" key={index}>
                    <div className="product-card center">
                        <a href="javascript:void(0)">
                            <img className="prod-img" title="fabric" src={prod.image ? prod.image : 'https://image.freepik.com/free-icon/dress-dummy_318-28505.jpg'} />
                            <p>{prod.name}</p>
                        </a>
                        <p className="currentPrice">Rs {prod.price}</p>
                    </div>
                </div> 
            )
        });
        return productsElements;
    },
    render: function() {
        var home_banner = null;
        if(this.state.staticContent && this.state.staticContent.home_banner){
            home_banner = this.state.staticContent.home_banner;
        }
        return(
            <div>
                <div className="container">
                    {home_banner ? <div className="row"><img className="banner" src={home_banner}/></div> : ""}
                    <br/>
                    {this.createProductHtml()}
                </div>
            </div>
        )
    }
});

module.exports = Products;