var React = require("react");
var ProductStore = require("../stores/ProductStore");
var ProductActions = require("../actions/ProductActions");

var Header = require("./Header.jsx");
var Footer = require("./Footer.jsx");

var Products = require("./Products.jsx");

function getAppState() {
    return {
        initialStateData: ProductStore.getInitialStateData(),
        products: ProductStore.getProducts(),
        staticContent: ProductStore.getStaticContent(),
    };
}

var Product = React.createClass({
    getInitialState: function() {
        return getAppState();
    },
    componentDidMount: function() {
        this.bindEvents();
        ProductActions.initDashboard();
    },
    componentWillUnmount: function() {
       this.removeEvents();
    },
    bindEvents: function(){
        ProductStore.addChangeListener('change', this._onChange);
    },
    removeEvents: function(){
        ProductStore.removeChangeListener('change', this._onChange);
    },
    _onChange: function() {
        this.setState(getAppState());
    },
    render: function() {
        return(
            <div>
                <Header initialStateData={this.state.initialStateData} />
                <Products initialStateData={this.state.initialStateData} products={this.state.products} staticContent={this.state.staticContent} />
                <br />
                <Footer />
            </div>
        )
    } 
});

module.exports = Product;