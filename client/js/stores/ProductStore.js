var AppDispatcher = require("../dispatcher/AppDispatcher");
var ProductConstants = require("../constants/ProductConstant");
var ProductApi = require("../utils/ProductApi");

var EventEmitter = require('events').EventEmitter;
var merge = require('merge');

var initialStateData = {}, products = [], staticContent = {};

function setInitialStateData(data){
    
    initialStateData = data;
    staticContent = data.staticContent;
    
    if(data.products){
        for(var categories in data.products){
            for(var subCategories in data.products[categories]){
                products = products.concat(data.products[categories][subCategories]);
            }
        }
    }
}

function loadProducts(prods){
    products = prods;
}

var ProductStore = merge(EventEmitter.prototype, {
    getInitialStateData: function() {
        return initialStateData;  
    },
    getProducts: function() {
        return products;  
    },
    getStaticContent: function() {
        return staticContent;  
    },
    emitChange: function(evt) {
        this.emit(evt);
    },
    addChangeListener: function(evt, callback) {
        this.on(evt, callback);
    },
    removeChangeListener: function(evt, callback) {
        this.removeListener(evt, callback);
    }
});

AppDispatcher.register(function(payload){
    var action = payload.action;
    switch(action.actionType) {
        case ProductConstants.INIT_DASHBOARD:
            setInitialStateData(action.data);
            break;
        case ProductConstants.LOAD_PRODUCTS:
            loadProducts(action.data);
            break;
        default:
            return true;
    }
    ProductStore.emitChange('change');
    return true;
});

module.exports = ProductStore;