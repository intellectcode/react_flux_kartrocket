Prerequisites
-------------

- [Node.js 4.0+](http://nodejs.org)


Getting Started
---------------

The easiest way to get started is to clone the repository:

# Get the latest snapshot
git clone https://intellectcode@bitbucket.org/intellectcode/react_flux_kartrocket.git

# Change directory
cd react_flux_kartrocket

# Install NPM dependencies
npm install

# Then simply start your app
npm start

Go to -> http://localhost:4000

Information
---------------
1. For displaying the data i have created the dummy json , - ProductApi which contains some products.
2. On Navigation Bar their are two options : Men , Women.
	2.1 Clicking on the sub categories will filter the displayed products.
3. For SEO friendly, page is been render from backend.
4. To show Infinite Scrolling , have appended the last item again and again. Till the list count becomes equal to 20.
5. Used Webpack as a Task Runner.
6. Webpack will generate a build with the name of build.js
7. ES5 is used as Javascript version
